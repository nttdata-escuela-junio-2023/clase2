package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Calculadora2 {

  private String numero1;

  private String numero2;

  public String suma() {
    return numero1 + numero2;
  }

  public String suma2() {
    return String.valueOf(Integer.parseInt(numero1) + Integer.parseInt(numero2));
  }
}
