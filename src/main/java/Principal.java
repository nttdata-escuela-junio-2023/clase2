import model.Calculadora;
import model.Calculadora2;
import model.Cliente;
import model3.Pedido;

public class Principal {

  public static void main(String[] args) {
    boolean flag = true;
    boolean flag2 = false;
    char letra = '1';
    int numero = -2147483648;
    long numerolargo = -2147483649L;
    short numerocorto = 32760;
    byte numerochiquito = 127;
    float numeroflotante = 100.5f;
    double numerodecimal = 10.5001;
    System.out.println(flag);
    String cadena = "cadena de texto";
    String[] listadecadenas = {"calete", "hola"};
    System.out.println(listadecadenas[1]);
    Pedido objecto = new Pedido();
    Boolean nuevoboleano = true;
    System.out.println(nuevoboleano.compareTo(flag2));

    //    Operadores arimeticos
    Calculadora calculadora = new Calculadora();
    calculadora.setNumero1(12);
    calculadora.numero2 = 8;
    System.out.println("suma: " + calculadora.suma());

    //operadores arimeticos incrementales
    Integer numeroAI = 5;
    System.out.println(numeroAI);

    numeroAI++;//numeroAI = numeroAI + 1
    System.out.println(numeroAI);
    numeroAI--;
    System.out.println(numeroAI);

    //operadores arimeticos combinados
    Integer numeroAC1 = 5;
    Integer numeroAC2 = 6;
    numeroAC2 = numeroAC1 + numeroAC2;
    System.out.println(numeroAC2);
    numeroAC2 += numeroAC1;
    System.out.println(numeroAC2);

    //operadores arimeticos comparacion y logicos
    Boolean result = numeroAC1 <= numeroAC2 && numeroAC1 > 0;
    System.out.println(result);

    //operador condicional o ternario
    Integer edad = 19;
    String cedena = edad > 18 ? "Soy mayor de edad" : "Soy menor de edad";
    System.out.println(cedena);

    //separadores
    Integer resultado = (19 + 5) * 2;
    System.out.println((double) resultado);

    //separadores
    System.out.println("probando la concatenacion de cadenas con el signo + " + calculadora.getNumero1() + calculadora.numero2 + "5");

    //PROBANDO
    Calculadora2 calculadora2 = new Calculadora2();
    calculadora2.setNumero1("1");
    calculadora2.setNumero2("5");
    System.out.println(calculadora2.suma());

    System.out.println(calculadora2.suma2());

    //SENTENCIAS IF
    String resultadoIf;
    if (edad > 18) {
      resultadoIf = "Soy mayor de edad";
    } else {
      resultadoIf = "Soy menor de edad";
    }
    resultadoIf = edad > 18 ? "Soy mayor de edad" : "Soy menor de edad";
    System.out.println(resultadoIf);

    //Cuando no podemos usar if ternarios
    Integer nroCompra = 3;
    Integer nroDevoluciones = 2;
    Pedido pedido = new Pedido();
    Cliente cliente = new Cliente();
    cliente.setNombre("Carloss");
    pedido.setCliente(cliente);
    pedido.setTipo(3);
    if (pedido.getTipo() == 1) {
      nroCompra++;//nroCompra = nroCompra +1
      System.out.println(nroCompra);
    } else if (pedido.getTipo() == 2) {
      nroDevoluciones++;
      System.out.println(nroDevoluciones);
    } else {
      System.out.println("No existe este tipo de pedidos");
    }

    //sentencia switch
    switch (String.valueOf(pedido.getTipo())) {
      case "1":
        nroCompra++;//nroCompra = nroCompra +1
        System.out.println(nroCompra);
        break;
      case "2":
        nroDevoluciones++;
        System.out.println(nroDevoluciones);
        break;
      default:
        System.out.println("No existe este tipo de pedidos");
        break;
    }

    //sentencia switch resumida para casos donde necesitamos realizar la misma accion para 2 o mas casos diferentes
    switch (String.valueOf(pedido.getTipo())) {
      case "1":
      case "2":
        System.out.println("Existe este tipo de pedidos");
        break;
      default:
        System.out.println("No existe este tipo de pedidos");
        break;
    }

    // For
    int indice;
    for (indice = 0; indice <= 10; indice++) {
      System.out.println(indice);
    }
    System.out.println(indice);

    // foreach
    String[] cadenas = {"1", "2", "3"};

    for (String elemento : cadenas) {
      System.out.println("el valor de este elemento es: " + elemento);
    }
    System.out.println("usando for tradicional para recorrer elementos de un array ");
    for (int indice2 = 0; indice2 < cadenas.length; indice2++) {
      System.out.println("el valor de este elemento es: " + cadenas[indice2]);
    }

    // while
    int i = 15;
    while (i < 15) {
      System.out.println("mensaje: " + i);
      i++;
    }

    //do while
    int index = 0;
    do {
      System.out.println("mensaje: " + index);
      index++;
    } while (index < 15);

    // usando static  y final
    System.out.println(Integer.parseInt("1154"));
    final Integer numeroTemp = 15;

    // Programacion orientada a objetos
    Cliente cliente1 = new Cliente();
    cliente1.setNombre("Leonardo");
    cliente1.setType(1);
    cliente1.setRuc("0000542222");
    Cliente cliente2 = cliente1;
    cliente2.setNombre("Gerardo");

    System.out.println(cliente1);
    System.out.println(cliente2);

    int var1 = 2;
    int var2 = var1;
    var2 = 3;

    System.out.println(var1);
    System.out.println(var2);

    // Parametros por referencia
    Cliente cliente3 = new Cliente();
    cliente3.setNombre("Leonardo");
    changeName(cliente3);
    System.out.println(cliente3);
  }

  static public void changeName(Cliente cliente) {
    cliente.setNombre("Carmelo");
  }
}
