package model3;

import java.util.List;
import lombok.Data;
import model.Cliente;
import model.Producto;

@Data
public class Pedido {

  private String nroOrden;

  private List<Producto> productos;

  private Cliente cliente;

  //1 compra , 2 devolucion
  private Integer tipo;

  private String getNroOrden() {
    return nroOrden;
  }

  private void setNroOrden(String nroOrden) {
    this.nroOrden = nroOrden;
  }

  private void showDetallePedido() {
    System.out.println(cliente.getNombre());
  }
}
